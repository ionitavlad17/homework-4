import React, { Component } from 'react'

class RobotForm extends Component{

    constructor(props){
        super(props)
        this.state={
            name: "",
            type: "",
            mass: ""
        }
        
        this.handleChange = (evt)=>{
            this.setState({
                 [evt.target.name] : evt.target.value
            })
        }
        this.handleClick=()=>
        {
            this.props.onAdd({
                name : this.state.name,
                type: this.state.type,
                mass: this.state.mass

            })
        }
        
    }
    
    
    render(){
        return <div>
        <input type="text" placeholder="name" id="name" name="name" onChange={this.handleChange}/>
         <input type="text" placeholder="type" name="type" id="type" onChange={this.handleChange}/>
         <input type="text" placeholder="mass" name="mass" id="mass" onChange={this.handleChange}/>
         <input type="button" value="add" onClick={this.handleClick}/>
        
        </div>
    }
}

export default RobotForm